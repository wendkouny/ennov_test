## Application de gestion de ticket

> C'est un système de gestion de tickets simple utilisant Java et Spring Framework. Le système doit permettre à un utilisateur de créer, lire, mettre à jour, affecter et supprimer des tickets. 
Chaque ticket doit avoir un titre, une description, un utilisateur assigné et un statut (en cours, terminé, annulé).
Pour l'utilisateur, on a juste besoin d'un username et d'un email. Un utilisateur peut avoir plusieurs tickets et un ticket ne peut être affecté qu'à un utilisateur.

### TAF

1. #### Partie 1 : Configuration du projet
   - Créez un nouveau projet Maven ou Gradle.
   - Configurez le projet avec Spring Boot.
   - Configurez la base de données H2 pour stocker les tickets.
   
2. #### Partie 2 : Modélisation des données
   - Créez les entités Java représentant les modèles d'objet :
      - Ticket (avec ses attributs)
      - User (avec ses attributs)
     
3. #### Partie 3 : Couches DAO et Service
   - Créez les interfaces TicketRepository et UserRepository pour gérer les opérations CRUD.
   - Implémentez cette interface en utilisant Spring Data JPA.
   - Implémenter les services nécessaires hors CRUD (voir partie 4)

4. #### Partie 4 : Contrôleurs REST
   - Créez un contrôleur REST UserController avec les points de terminaison suivants :
     - GET /users: Récupérer tous les utilisateurs
     - GET /users/{id}/ticket: Récupérer les tickets assignés à l'utilisateur
     - POST /users: Créer un utilisateur
     - PUT /users/{id}: Modifier un utilisateur

   - Créez un contrôleur REST TicketController avec les points de terminaison suivants :
       - GET /tickets: Récupérer tous les tickets.
       - GET / tickets /{id}: Récupérer un ticket par son ID.
       - POST / tickets: Créer un nouveau ticket.
       - PUT / tickets /{id}: Mettre à jour un ticket existant.
       - PUT / tickets /{id}/assign/{useId}: Assigner un ticket à un utilisateur.
       - DELETE / tickets /{id}: Supprimer un ticket par son ID.
     - Contrôle d'accès
         - Ajoutez une fonctionnalité pour restreindre l'accès aux tickets en fonction de l'utilisateur connecté
       
> Pour cette fonctionnalité, après plusieurs hésitation, j'ai implémenté de façon basic spring security.
Cela a permet de faire un test concret en récupérant l'utilisateur connecté. 
Un service service de connexion a été implémenté pour le test de cette fonctionnalité.


5. #### Partie 5 : Tests
   - Écrivez des tests unitaires pour tester les fonctionnalités des services et des contrôleurs.
   - Utilisez JUnit et Mockito pour les tests.
   
> L'essentiel des tests controllers et services ont été faits mais la couverture n'a pas atteint 100%.
La partie config spring security n'étant pas pris en compte dans les tests unitaires.

6. #### Partie 6 : Documentation
   - Documentez vos APIs REST à l'aide de Swagger ou OpenAPI.
   - Assurez-vous de gérer les cas d'erreur appropriés, comme la gestion des identifiants inexistants ou d'autres erreurs de validation, en renvoyant les réponses HTTP appropriées.
   - L'utilisation du "Generic" et les "Design patterns" serait un plus dans votre implémentation 

### Comment tester ?

**NB :**  _Le code est compilé sur du java 17_

> On peut se servir de postman ou insomnia ou IntelliJ(**_Ce que j'ai fait_**). 
Si on opte pour l'un de ces outils on peut se servir du fichier [/data/ennov_endpoints.http](/data/ennov_endpoints.http).
> 
>L'autre moyen est passer par swagger à l'adresse suivante : [http://localhost:8092/swagger-ui/index.html](http://localhost:8092/swagger-ui/index.html)
> 
> On a aussi un fichier [/data/query.sql](/data/query.sql) pour qu'il faut utiliser pour enregistrer un utilisateur pour le test de la fonction de control d'accès.
> 
> On peut soit son votre IDE créer une datasource H2 ou soit acceder à la console H2 à partir de votre navigateur préféré à l'adresse [http://localhost:8092/h2-console/](http://localhost:8092/h2-console/)
> 
> _Avant tout ça il faut lancer les TU et ensuite demarrer le service_
> 
> - `mvn clean install`
> - `mvn spring-boot:run`