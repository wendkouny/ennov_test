package test.projects.ennov.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import test.projects.ennov.config.security.TestUserDetailsService;
import test.projects.ennov.config.security.jwt.AuthTokenFilter;
import test.projects.ennov.config.security.jwt.TokenProvider;
import test.projects.ennov.domain.User;
import test.projects.ennov.dto.AccountDto;
import test.projects.ennov.dto.TicketDto;
import test.projects.ennov.dto.UserDto;
import test.projects.ennov.dto.mapper.TicketMapper;
import test.projects.ennov.dto.mapper.UserMapper;
import test.projects.ennov.repository.TicketRepository;
import test.projects.ennov.repository.UserRepository;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UserService {
    private final UserMapper mapper;
    private final UserRepository userRepository;
    private final TicketRepository ticketRepository;
    private final TicketMapper ticketMapper;
    private final TokenProvider tokenProvider;
    private final AuthenticationManager authenticationManager;
    private final TestUserDetailsService userDetailsService;

    /**
     * Save user.
     *
     * @param userDto {@link test.projects.ennov.dto.UserDto}
     * @return saved user object
     */
    private UserDto saveUser(final UserDto userDto) {
        User user = mapper.toEntity(userDto);
        user.setDeleted(Boolean.FALSE);

        User savedUser = userRepository.save(user);

        return mapper.toDto(savedUser);
    }

    /**
     * Create new user.
     *
     * @param userDto {@link test.projects.ennov.dto.UserDto}
     * @return created user object
     */
    public UserDto createUser(final UserDto userDto) {
        if (userRepository.existsByDeletedFalseAndEmailOrUsername(userDto.getEmailAddress(), userDto.getLongin())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User exists");
        }

        return saveUser(userDto);
    }

    /**
     * Update existing user.
     *
     * @param userDto {@link test.projects.ennov.dto.UserDto}
     * @return updated user object
     */
    public UserDto updateUser(final UserDto userDto, final long id) {
        if (!userRepository.existsByDeletedFalseAndId(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("No user exists with this ID : %d.", id));
        }

        if (Objects.isNull(userDto.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Existing user cannot have null ID.");
        }

        return saveUser(userDto);
    }


    /**
     * Get user by id.
     *
     * @param id searched user id
     * @return found user object
     */
    public UserDto findUser(final long id) {
        if (!userRepository.existsByDeletedFalseAndId(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("No user exists with this ID : %d", id));
        }

        return mapper.toDto(userRepository.findOneByDeletedFalseAndId(id));
    }

    /**
     * Fetch all user stored in DB.
     *
     * @return list of {@link test.projects.ennov.dto.UserDto}
     */
    public List<UserDto> findAllUser() {
        List<User> users = userRepository.findAllByDeletedFalse();
        if (users.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "No data found, Please create at least one user before.");
        }
        return mapper.toDtos(users);
    }

    /**
     * Fetch ticket list belong to user.
     *
     * @param userId user id
     * @return list of {@link test.projects.ennov.dto.UserDto}
     */
    public List<TicketDto> fetchUserByUser(final long userId) {
        Optional<User> user = userRepository.findTop1ByDeletedFalseAndId(userId);
        if (user.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("No user exists with this id : %s", userId));
        }

        return ticketMapper.toDtos(ticketRepository.findAllByDeletedFalseAndUserId(user.get().getId()));
    }

    /**
     * Login.
     *
     * @param accountDto
     * @return Void
     */
    public ResponseEntity<Void> login(final AccountDto accountDto) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(accountDto.getLogin(), accountDto.getPassword());

        log.debug("Auth token : {}", authenticationToken.getCredentials());

        Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final UserDetails userDetails = userDetailsService.loadUserByUsername(accountDto.getLogin());

        String jwt = tokenProvider.generateJwtToken(userDetails);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(AuthTokenFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        boolean result = userRepository.existsByDeletedFalseAndUsername(accountDto.getLogin());

        if (!result) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad credentials, Login or password incorrect.");
        }
        return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    }

}
