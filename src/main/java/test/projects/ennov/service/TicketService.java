package test.projects.ennov.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import test.projects.ennov.config.security.SecurityUtils;
import test.projects.ennov.domain.Ticket;
import test.projects.ennov.domain.User;
import test.projects.ennov.domain.enums.TicketStatus;
import test.projects.ennov.dto.TicketDto;
import test.projects.ennov.dto.mapper.TicketMapper;
import test.projects.ennov.repository.TicketRepository;
import test.projects.ennov.repository.UserRepository;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class TicketService {
    private final TicketRepository ticketRepository;
    private final TicketMapper mapper;
    private final UserRepository userRepository;

    /**
     * Save ticket.
     *
     * @param ticketDto {@link test.projects.ennov.dto.TicketDto}
     * @return saved ticket object
     */
    private TicketDto saveTicket(final TicketDto ticketDto) {
        Ticket ticket = mapper.toEntity(ticketDto);
        ticket.setDeleted(Boolean.FALSE);

        if (Objects.isNull(ticket.getId())) {
            ticket.setStatus(TicketStatus.READY);
        }

        Ticket savedTicket = ticketRepository.save(ticket);

        return mapper.toDto(savedTicket);
    }

    /**
     * Create new ticket.
     *
     * @param ticketDto {@link test.projects.ennov.dto.TicketDto}
     * @return created ticket object
     */
    public TicketDto createTicket(final TicketDto ticketDto) {
        if (ticketRepository.existsByDeletedFalseAndTitle(ticketDto.getTicketTitle())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Ticket already exists.");
        }

        return saveTicket(ticketDto);
    }

    /**
     * Update existing ticket.
     *
     * @param ticketDto {@link test.projects.ennov.dto.TicketDto}
     * @return updated ticket object
     */
    public TicketDto updateTicket(final TicketDto ticketDto, final long id) {
        if (!ticketRepository.existsByDeletedFalseAndId(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("No ticket exists with this ID : %d", id));
        }

        if (Objects.isNull(ticketDto.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Already created ticket cannot have null ID.");
        }

        if (ticketRepository.existsByDeletedFalseAndTitleAndIdNot(ticketDto.getTicketTitle(), id)) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "A Ticket with the same title already exists.");
        }

        return saveTicket(ticketDto);
    }


    /**
     * Get ticket by id.
     *
     * @param id searched ticket id
     * @return found ticket object
     */
    public TicketDto findTicket(final long id) {
        if (!ticketRepository.existsByDeletedFalseAndId(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("No ticket exists with this ID : %d", id));
        }

        return mapper.toDto(ticketRepository.findOneByDeletedFalseAndId(id));
    }

    /**
     * Fetch all ticket stored in DB.
     *
     * @return list of {@link test.projects.ennov.dto.TicketDto}
     */
    public List<TicketDto> findAllTicket() {
        List<Ticket> tickets = ticketRepository.findAllByDeletedFalse();
        if (tickets.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "No data found, Please create at least one ticket before.");
        }
        return mapper.toDtos(tickets);
    }

    /**
     * Remove a ticket by id if exists.
     *
     * @param id removed ticket id.
     */
    public void deleteTicket(final long id) {
        ticketRepository.findTop1ByDeletedFalseAndId(id).ifPresentOrElse(ticket -> {
            ticket.setDeleted(Boolean.TRUE);
            ticketRepository.save(ticket);
        }, () -> {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Cannot remove ticket with ID : %d", id));
        });
    }

    /**
     * Assign not already closed ticket to a user.
     *
     * @param ticketId ticket id
     * @param userId   user id
     */
    public void assignTicket(final long ticketId, final long userId) {
        if (!userRepository.existsByDeletedFalseAndId(userId)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("No user exists with this ID : %d", userId));
        }

        if (ticketRepository.existsByDeletedFalseAndStatusAndId(TicketStatus.CANCElED, ticketId)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Cannot assign again a closed ticket.");
        }

        Ticket ticket = ticketRepository.getReferenceById(ticketId);
        ticket.setUser(User.builder().id(userId).build());
        ticketRepository.save(ticket);
    }

    /**
     * Fetch ticket list belong to current user.
     *
     * @return list of {@link test.projects.ennov.dto.TicketDto}
     */
    @PreAuthorize("authenticated")
    public List<TicketDto> fetchTicketByCurrentUser() {
        String userLogin = SecurityUtils.getCurrentUsername();
        if (userLogin.equals("Anonymous")) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are not logged in.");
        }
        User user = userRepository.findTop1ByDeletedFalseAndUsername(userLogin);

        return mapper.toDtos(ticketRepository.findAllByDeletedFalseAndUserId(user.getId()));
    }

}
