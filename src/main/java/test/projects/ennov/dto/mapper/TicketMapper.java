package test.projects.ennov.dto.mapper;

import org.springframework.stereotype.Component;
import test.projects.ennov.domain.Ticket;
import test.projects.ennov.domain.User;
import test.projects.ennov.dto.TicketDto;
import java.util.List;

@Component
public class TicketMapper {
    public TicketDto toDto(Ticket ticket) {
        return TicketDto.builder()
                .id(ticket.getId())
                .ticketTitle(ticket.getTitle())
                .ticketDesc(ticket.getDescription())
                .ticketStatus(ticket.getStatus())
                .userId(ticket.getUser().getId())
                .createUser(ticket.getCreatedBy())
                .dateCreation(ticket.getCreatedDate())
                .lastModifyUser(ticket.getLastModifiedBy())
                .dateLastModification(ticket.getLastModifiedDate())
                .isDeleted(ticket.getDeleted())
                .build();
    }

    public Ticket toEntity(TicketDto ticketDto) {
        return Ticket.builder()
                .id(ticketDto.getId())
                .title(ticketDto.getTicketTitle())
                .description(ticketDto.getTicketDesc())
                .status(ticketDto.getTicketStatus())
                .user(User.builder()
                        .id(ticketDto.getUserId())
                        .build())
                .build();
    }

    public List<TicketDto> toDtos(List<Ticket> tickets) {
        return tickets.stream().map(this::toDto).toList();
    }

    public List<Ticket> toEntities(List<TicketDto> ticketDtos) {
        return ticketDtos.stream().map(this::toEntity).toList();
    }
}
