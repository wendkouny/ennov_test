package test.projects.ennov.dto.mapper;

import org.springframework.stereotype.Component;
import test.projects.ennov.domain.User;
import test.projects.ennov.dto.UserDto;
import java.util.List;

@Component
public class UserMapper {
    public UserDto toDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .longin(user.getUsername())
                .emailAddress(user.getEmail())
                .createUser(user.getCreatedBy())
                .dateCreation(user.getCreatedDate())
                .lastModifyUser(user.getLastModifiedBy())
                .dateLastModification(user.getLastModifiedDate())
                .isDeleted(user.getDeleted())
                .build();
    }

    public User toEntity(UserDto userDto) {
        return User.builder()
                .id(userDto.getId())
                .username(userDto.getLongin())
                .email(userDto.getEmailAddress())
                .build();
    }

    public List<UserDto> toDtos(List<User> users) {
        return users.stream().map(this::toDto).toList();
    }

    public List<User> toEntities(List<UserDto> userDtos) {
        return userDtos.stream().map(this::toEntity).toList();
    }
}
