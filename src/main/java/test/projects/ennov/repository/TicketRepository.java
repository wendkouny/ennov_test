package test.projects.ennov.repository;

import test.projects.ennov.domain.Ticket;
import test.projects.ennov.domain.enums.TicketStatus;
import java.util.List;

public interface TicketRepository extends AbstractRepository<Ticket, Long> {
    boolean existsByDeletedFalseAndTitle(String title);
    boolean existsByDeletedFalseAndTitleAndIdNot(String title, Long id);
    boolean existsByDeletedFalseAndStatusAndId(TicketStatus status, Long id);

    List<Ticket> findAllByDeletedFalseAndUserId(Long userId);
}
