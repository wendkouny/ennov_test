package test.projects.ennov.repository;

import test.projects.ennov.domain.User;
import java.util.Optional;

public interface UserRepository extends AbstractRepository<User, Long> {
    Optional<User> findByDeletedFalseAndUsername(String username);
    User findTop1ByDeletedFalseAndUsername(String username);

    boolean existsByDeletedFalseAndEmailOrUsername(String email, String username);
    boolean existsByDeletedFalseAndUsername(String username);

    boolean existsByDeletedFalseAndEmailOrUsernameAndIdNot(String email, String username, Long id);
}
