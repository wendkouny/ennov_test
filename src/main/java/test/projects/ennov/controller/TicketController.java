package test.projects.ennov.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import test.projects.ennov.dto.TicketDto;
import test.projects.ennov.service.TicketService;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/tickets")
@RequiredArgsConstructor
@Tags(@Tag(name = "Tickets", description = "Gestion des tickets"))
public class TicketController {
    private final TicketService ticketService;

    /**
     * POST  /tickets  : Creates a new ticket.
     *
     * @param ticketDto
     * @return {@link test.projects.ennov.dto.TicketDto}
     */
    @PostMapping("/")
    @Operation(summary = "Creating a new ticket.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "404", description = "${swagger.http-status.404}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<TicketDto> createTicket(@Valid @RequestBody final TicketDto ticketDto) {
        return ResponseEntity.ok(ticketService.createTicket(ticketDto));
    }

    /**
     * PUT  /tickets/:id  : Updates an existing Ticket.
     *
     * @param ticketDto
     * @param id
     * @return {@link test.projects.ennov.dto.TicketDto}
     */
    @PutMapping("/{id}")
    @Operation(summary = "Update an existing ticket.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "400", description = "${swagger.http-status.400}"),
            @ApiResponse(responseCode = "404", description = "${swagger.http-status.404}"),
            @ApiResponse(responseCode = "409", description = "${swagger.http-status.409}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<TicketDto> updateTicket(@Valid @RequestBody final TicketDto ticketDto, @PathVariable Long id) {
        return ResponseEntity.ok(ticketService.updateTicket(ticketDto, id));
    }

    /**
     * GET / : get all tickets.
     *
     * @return {@link java.util.List<test.projects.ennov.dto.TicketDto>}
     */
    @GetMapping("/")
    @Operation(summary = "Fetch all tickets")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "204", description = "${swagger.http-status.204}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<List<TicketDto>> getAllTickets() {
        return new ResponseEntity<>(ticketService.findAllTicket(), HttpStatus.OK);
    }

    /**
     * GET /:id : get ticket.
     *
     * @param id
     * @return {@link java.util.List<test.projects.ennov.dto.TicketDto>}
     */
    @GetMapping("/{id}")
    @Operation(summary = "Get ticket")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "404", description = "${swagger.http-status.404}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<TicketDto> findTicket(@PathVariable final Long id) {
        return ResponseEntity.ok(ticketService.findTicket(id));
    }

    /**
     * DELETE /:id : delete ticket.
     *
     * @param id
     * @return {@link java.util.List<test.projects.ennov.dto.TicketDto>}
     */
    @DeleteMapping("/{id}")
    @Operation(summary = "Remove ticket")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "400", description = "${swagger.http-status.400}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<Void> deleteTicket(@PathVariable final Long id) {
        ticketService.deleteTicket(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * PUT /:id/assign/:useId : Assign ticket to user.
     *
     * @param id
     * @return {@link java.util.List< test.projects.ennov.dto.TicketDto>}
     */
    @PutMapping("/{id}/assign/{userId}")
    @Operation(summary = "Assign ticket to user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "400", description = "${swagger.http-status.400}"),
            @ApiResponse(responseCode = "403", description = "${swagger.http-status.403}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<Void> asignTicketToUser(@PathVariable final Long id,
                                                  @PathVariable final Long userId) {
        ticketService.assignTicket(id, userId);
        return ResponseEntity.noContent().build();
    }

    /**
     * GET /user : get current user assigned tickets.
     *
     * @return {@link java.util.List<test.projects.ennov.dto.TicketDto>}
     */
    @GetMapping("/user")
    @Operation(summary = "Current user assigned tickets")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "403", description = "${swagger.http-status.403}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<List<TicketDto>> fetchCurrentUserTickets() {
        return new ResponseEntity<>(ticketService.fetchTicketByCurrentUser(), HttpStatus.OK);
    }
}
