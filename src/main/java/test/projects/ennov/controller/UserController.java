package test.projects.ennov.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import test.projects.ennov.dto.AccountDto;
import test.projects.ennov.dto.TicketDto;
import test.projects.ennov.dto.UserDto;
import test.projects.ennov.repository.UserRepository;
import test.projects.ennov.service.UserService;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
@Tags(@Tag(name = "Utilisateurs", description = "Gestion des utilisateurs"))
public class UserController {
    private final UserService userService;
    private final UserRepository userRepository;

    /**
     * POST  /users  : Creates a new user.
     *
     * @param userDto
     * @return {@link UserDto}
     */
    @PostMapping("/")
    @Operation(summary = "Creating a new user.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "400", description = "${swagger.http-status.400}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody final UserDto userDto) {
        return ResponseEntity.ok(userService.createUser(userDto));
    }

    /**
     * PUT  /users/:id  : Updates an existing User.
     *
     * @param userDto
     * @param id
     * @return {@link UserDto}
     */
    @PutMapping("/{id}")
    @Operation(summary = "Update an existing user.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "400", description = "${swagger.http-status.400}"),
            @ApiResponse(responseCode = "404", description = "${swagger.http-status.404}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<UserDto> updateUser(@Valid @RequestBody final UserDto userDto, @PathVariable long id) {
        return ResponseEntity.ok(userService.updateUser(userDto, id));
    }

    /**
     * GET / : get all users.
     *
     * @return {@link List<UserDto>}
     */
    @GetMapping("/")
    @Operation(summary = "Fetch all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "204", description = "${swagger.http-status.204}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return new ResponseEntity<>(userService.findAllUser(), HttpStatus.OK);
    }

    /**
     * GET /:id : get user.
     *
     * @param id
     * @return {@link List<UserDto>}
     */
    @GetMapping("/{id}")
    @Operation(summary = "Get user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "404", description = "${swagger.http-status.404}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<UserDto> findUser(@PathVariable final long id) {
        return ResponseEntity.ok(userService.findUser(id));
    }

    /**
     * GET /:id/ticket : get all ticket by user.
     *
     * @param id
     * @return {@link List<TicketDto>}
     */
    @GetMapping("/{id}/ticket")
    @Operation(summary = "Fetch all ticket belong user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "400", description = "${swagger.http-status.400}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<List<TicketDto>> getAllTicketByUser(@PathVariable final long id) {
        return new ResponseEntity<>(userService.fetchUserByUser(id), HttpStatus.OK);
    }


    /**
     * Login.
     *
     * @param accountDto
     * @return void
     */
    @PostMapping("/login")
    @Operation(summary = "Login.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "404", description = "${swagger.http-status.404}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    private ResponseEntity<Void> login(@Valid @RequestBody final AccountDto accountDto) {
        if (!userRepository.existsByDeletedFalseAndUsername(accountDto.getLogin())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Account not found.");
        }

        return userService.login(accountDto);
    }

}
