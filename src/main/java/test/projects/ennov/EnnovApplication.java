package test.projects.ennov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnnovApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnnovApplication.class, args);
    }

}
