package test.projects.ennov.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import test.projects.ennov.domain.enums.TicketStatus;
import test.projects.ennov.dto.TicketDto;
import test.projects.ennov.dto.UserDto;
import test.projects.ennov.service.TicketService;
import test.projects.ennov.service.UserService;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserControllerTests {
    @InjectMocks
    UserController userController;

    @Mock
    UserService userService;

    @Mock
    private TicketService ticketService;

    @BeforeAll
    public static void init() {
        MockitoAnnotations.openMocks(UserControllerTests.class);
    }

    @Test
    public void testCreateUser_thenOK() {
        UserDto userDto = UserDto.builder().id(1000L).longin("dieudo").emailAddress("dieudo.dev.bf").build();

        when(userService.createUser(any())).thenReturn(userDto);
        ResponseEntity<UserDto> responseEntity = userController.createUser(any());

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.hasBody());
        assertEquals(userDto, responseEntity.getBody());
    }

    @Test
    public void testUpdateUser_thenOK() {
        UserDto userDto = UserDto.builder().id(1000L).longin("dieudo").emailAddress("dieudo.dev.bf").build();

        when(userService.updateUser(userDto, 1000L)).thenReturn(userDto);
        ResponseEntity<UserDto> responseEntity = userController.updateUser(userDto, 1000L);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.hasBody());
        assertEquals(userDto, responseEntity.getBody());
    }

    @Test
    public void testGetUser_thenOK() {
        UserDto userDto = UserDto.builder().id(1000L).longin("dieudo").emailAddress("dieudo.dev.bf").build();

        when(userService.findUser(1000L)).thenReturn(userDto);
        ResponseEntity<UserDto> responseEntity = userController.findUser(1000L);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.hasBody());
        assertEquals(userDto, responseEntity.getBody());
    }

    @Test
    public void testGetAllUser_thenOK() {
        List<UserDto> userDtos = Collections.singletonList(UserDto.builder().id(1000L).longin("dieudo").emailAddress("dieudo.dev.bf").build());

        when(userService.findAllUser()).thenReturn(userDtos);
        ResponseEntity<List<UserDto>> responseEntity = userController.getAllUsers();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.hasBody());
        assertEquals(1, Objects.requireNonNull(responseEntity.getBody()).size());
    }

    @Test
    public void testFindAllUser_thenOK() {
        List<TicketDto> ticketDtos = Collections.singletonList(TicketDto.builder().id(1001L).ticketTitle("Test ticket").ticketDesc("desc").ticketStatus(TicketStatus.READY).build());

        when(this.userService.fetchUserByUser(1000L)).thenReturn(ticketDtos);
        ResponseEntity<List<TicketDto>> responseEntity = userController.getAllTicketByUser(1000L);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.hasBody());
        assertEquals(1, Objects.requireNonNull(responseEntity.getBody()).size());
    }

}
