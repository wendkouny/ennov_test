package test.projects.ennov.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import test.projects.ennov.TestUtils;
import test.projects.ennov.domain.enums.TicketStatus;
import test.projects.ennov.dto.TicketDto;
import test.projects.ennov.service.TicketService;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TicketControllerTests {
    @InjectMocks
    TicketController ticketController;

    @Mock
    TicketService ticketService;

    @BeforeAll
    public static void init() {
        MockitoAnnotations.openMocks(TicketControllerTests.class);
    }

    @Test
    public void testCreateTicket_thenOK() {
        TicketDto ticketDto = TicketDto.builder()
                .id(1001L).ticketTitle("Test ticket").isDeleted(Boolean.FALSE)
                .ticketDesc("desc").ticketStatus(TicketStatus.READY).build();

        when(ticketService.createTicket(any())).thenReturn(ticketDto);
        ResponseEntity<TicketDto> responseEntity = ticketController.createTicket(any());

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.hasBody());
        assertEquals(ticketDto, responseEntity.getBody());
    }

    @Test
    public void testUpdateTicket_thenOK() {
        TicketDto ticketDto = TicketDto.builder()
                .id(1001L).ticketTitle("Test ticket").isDeleted(Boolean.FALSE)
                .ticketDesc("desc").ticketStatus(TicketStatus.READY).build();

        when(ticketService.updateTicket(ticketDto, 1001L)).thenReturn(ticketDto);
        ResponseEntity<TicketDto> responseEntity = ticketController.updateTicket(ticketDto, 1001L);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.hasBody());
        assertEquals(ticketDto, responseEntity.getBody());
    }

    @Test
    public void testGetTicket_thenOK() {
        TicketDto ticketDto = TicketDto.builder()
                .id(1001L).ticketTitle("Test ticket").isDeleted(Boolean.FALSE)
                .ticketDesc("desc").ticketStatus(TicketStatus.READY).build();

        when(ticketService.findTicket(1001L)).thenReturn(ticketDto);
        ResponseEntity<TicketDto> responseEntity = ticketController.findTicket(1001L);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.hasBody());
        assertEquals(ticketDto, responseEntity.getBody());
    }

    @Test
    public void testGetAllTicket_thenOK() {
        List<TicketDto> ticketDtos = Collections.singletonList(TicketDto.builder()
                .id(1001L).ticketTitle("Test ticket").isDeleted(Boolean.FALSE)
                .ticketDesc("desc").ticketStatus(TicketStatus.READY).build());

        when(ticketService.findAllTicket()).thenReturn(ticketDtos);
        ResponseEntity<List<TicketDto>> responseEntity = ticketController.getAllTickets();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.hasBody());
        assertEquals(1, Objects.requireNonNull(responseEntity.getBody()).size());
    }

    @Test
    public void testDeleteTicket_thenOK() {
        ResponseEntity<Void> responseEntity = ticketController.deleteTicket(1001L);
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }

    @Test
    public void testAssignTicket_thenOK() {
        ResponseEntity<Void> responseEntity = ticketController.asignTicketToUser(1000L, 1001L);
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }

    @Test
    public void TestFetchByCurrentUser_thenOK() {
        TestUtils.login();

        List<TicketDto> tickets = List.of(
                TicketDto.builder().id(1000L).ticketTitle("Test Ticket 1").ticketDesc("desc 1").isDeleted(Boolean.FALSE).userId(1001L).build(),
                TicketDto.builder().id(1001L).ticketTitle("Test Ticket 2").ticketDesc("desc 2").isDeleted(Boolean.FALSE).userId(1001L).build()
        );
        when(ticketService.fetchTicketByCurrentUser()).thenReturn(tickets);
        ResponseEntity<List<TicketDto>> responseEntity = ticketController.fetchCurrentUserTickets();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.hasBody());
        assertEquals(2, Objects.requireNonNull(responseEntity.getBody()).size());
    }

}
