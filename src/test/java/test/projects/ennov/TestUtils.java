package test.projects.ennov;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class TestUtils {

    public static void login() {

        UserDetails userDetails = org.springframework.security.core.userdetails.User.builder()
                .username("dbadmin").password("admin")
                .authorities(new SimpleGrantedAuthority("ADMIN")).build();

        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
