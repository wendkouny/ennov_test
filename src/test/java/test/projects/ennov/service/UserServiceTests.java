package test.projects.ennov.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.server.ResponseStatusException;
import test.projects.ennov.config.security.TestUserDetailsService;
import test.projects.ennov.config.security.jwt.AuthTokenFilter;
import test.projects.ennov.config.security.jwt.TokenProvider;
import test.projects.ennov.domain.Ticket;
import test.projects.ennov.domain.User;
import test.projects.ennov.dto.AccountDto;
import test.projects.ennov.dto.UserDto;
import test.projects.ennov.dto.mapper.TicketMapper;
import test.projects.ennov.dto.mapper.UserMapper;
import test.projects.ennov.repository.TicketRepository;
import test.projects.ennov.repository.UserRepository;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class UserServiceTests {
    @InjectMocks
    UserService userService;

    @Mock
    private UserMapper mapper;
    @Mock
    private UserRepository userRepository;
    @Mock
    private TicketRepository ticketRepository;
    @Mock
    private TicketMapper ticketMapper;
    @Mock
    private TokenProvider tokenProvider;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private TestUserDetailsService userDetailsService;

    @Captor
    ArgumentCaptor<List<Ticket>> ticketListCaptor;

    @BeforeAll
    public static void init() {
        MockitoAnnotations.openMocks(UserServiceTests.class);
    }

    @Test
    public void testCreateUser_thenOK() {
        User user = User.builder().username("dieudo").email("dieudo.dev.bf").build();
        UserDto userDto = UserDto.builder().longin("dieudo").emailAddress("dieudo.dev.bf").build();

        when(this.mapper.toEntity(userDto)).thenReturn(user);
        when(this.userRepository.existsByDeletedFalseAndEmailOrUsername(anyString(), anyString())).thenReturn(false);

        this.userService.createUser(userDto);
        verify(this.userRepository, times(1)).save(user);
    }

    @Test
    public void testCreateUser_thenKO() {
        User user = User.builder().username("dieudo").email("dieudo.dev.bf").build();
        UserDto userDto = UserDto.builder().longin("dieudo").emailAddress("dieudo.dev.bf").build();
        when(this.mapper.toEntity(userDto)).thenReturn(user);
        when(this.userRepository.existsByDeletedFalseAndEmailOrUsername(anyString(), anyString())).thenReturn(true);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> this.userService.createUser(userDto));
        verify(this.userRepository, times(0)).save(any(User.class));

        Assertions.assertInstanceOf(ResponseStatusException.class, exception);
        Assertions.assertEquals(400, ((ResponseStatusException) exception).getBody().getStatus());
    }

    @Test
    public void testUpdateUser_thenOK() {
        long id = 1001L;

        User user = User.builder().id(id).username("dieudo").email("dieudo.dev.bf").build();
        UserDto userDto = UserDto.builder().id(id).longin("dieudo").emailAddress("dieudo.dev.bf").build();

        when(this.mapper.toEntity(userDto)).thenReturn(user);
        when(this.userRepository.existsByDeletedFalseAndId(id)).thenReturn(true);

        this.userService.updateUser(userDto, id);
        verify(this.userRepository, times(1)).save(user);
    }

    @Test
    public void testUpdateUser_thenKO_404() {
        long id = 1001L;

        User user = User.builder().id(id).username("dieudo").email("dieudo.dev.bf").build();
        UserDto userDto = UserDto.builder().id(id).longin("dieudo").emailAddress("dieudo.dev.bf").build();

        when(this.mapper.toEntity(userDto)).thenReturn(user);
        when(this.userRepository.existsByDeletedFalseAndId(id)).thenReturn(false);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> this.userService.updateUser(userDto, id));
        verify(this.userRepository, times(0)).save(any(User.class));

        Assertions.assertEquals(404, ((ResponseStatusException) exception).getBody().getStatus());
        Assertions.assertEquals("\"No user exists with this ID : 1001.\"", exception.getMessage()
                .substring("404 NOT_FOUND ".length()));
    }

    @Test
    public void testUpdateUser_thenKO_400() {

        User user = User.builder().username("dieudo").email("dieudo.dev.bf").build();
        UserDto userDto = UserDto.builder().longin("dieudo").emailAddress("dieudo.dev.bf").build();

        when(this.mapper.toEntity(userDto)).thenReturn(user);
        when(this.userRepository.existsByDeletedFalseAndId(anyLong())).thenReturn(true);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> this.userService.updateUser(userDto, anyLong()));
        verify(this.userRepository, times(0)).save(any(User.class));

        Assertions.assertEquals(400, ((ResponseStatusException) exception).getBody().getStatus());
        Assertions.assertEquals("\"Existing user cannot have null ID.\"", exception.getMessage()
                .substring("400 BAD_REQUEST ".length()));
    }

    @Test
    public void testFetchByUser_thenOK() {
        long id = 1001L;
        User user = User.builder().id(id).username("dieudo").email("dieudo.dev.bf").build();
        when(this.userRepository.findTop1ByDeletedFalseAndId(anyLong())).thenReturn(Optional.of(user));

        List<Ticket> tickets = List.of(
                Ticket.builder().id(1000L).title("Test Ticket 1").description("desc 1").deleted(Boolean.FALSE).build(),
                Ticket.builder().id(1001L).title("Test Ticket 2").description("desc 2").deleted(Boolean.FALSE).build()
        );

        when(this.ticketRepository.findAllByDeletedFalseAndUserId(id)).thenReturn(tickets);
        this.userService.fetchUserByUser(id);

        verify(this.ticketRepository, times(1)).findAllByDeletedFalseAndUserId(user.getId());
        verify(this.ticketMapper, times(1)).toDtos(ticketListCaptor.capture());

        Assertions.assertEquals(2, ticketListCaptor.getValue().size());
    }

    @Test
    public void testFetchByUser_thenKO() {
        when(this.userRepository.findTop1ByDeletedFalseAndId(anyLong())).thenReturn(Optional.empty());

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> this.userService.fetchUserByUser(anyLong()));
        Assertions.assertEquals(404, ((ResponseStatusException) exception).getBody().getStatus());
    }

    @Test
    public void TestLogin_thenOK() {
        AccountDto accountDto = AccountDto.builder().login("admin").password("admin").build();
        UserDetails userDetails = mock(UserDetails.class);
        when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class))).thenReturn(mock(Authentication.class));
        when(userDetailsService.loadUserByUsername(accountDto.getLogin())).thenReturn(userDetails);
        when(tokenProvider.generateJwtToken(userDetails)).thenReturn("mockToken");
        when(userRepository.existsByDeletedFalseAndUsername(accountDto.getLogin())).thenReturn(true);

        ResponseEntity<Void> response = userService.login(accountDto);

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertTrue(response.getHeaders().containsKey(AuthTokenFilter.AUTHORIZATION_HEADER));
    }

    @Test
    public void TestLogin_thenKO() {
        AccountDto accountDto = AccountDto.builder().login("admin").password("admin").build();
        UserDetails userDetails = mock(UserDetails.class);
        when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class))).thenReturn(mock(Authentication.class));
        when(userDetailsService.loadUserByUsername(accountDto.getLogin())).thenReturn(userDetails);
        when(tokenProvider.generateJwtToken(userDetails)).thenReturn("fakeJwt");
        when(userRepository.existsByDeletedFalseAndUsername(accountDto.getLogin())).thenReturn(false);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> userService.login(accountDto));

        Assertions.assertEquals(400, ((ResponseStatusException) exception).getBody().getStatus());
        Assertions.assertTrue(exception.getMessage().contains("Bad credentials, Login or password incorrect."));
    }

}
