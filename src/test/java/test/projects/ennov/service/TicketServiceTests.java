package test.projects.ennov.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.server.ResponseStatusException;
import test.projects.ennov.TestUtils;
import test.projects.ennov.config.security.jwt.TokenProvider;
import test.projects.ennov.domain.Ticket;
import test.projects.ennov.domain.User;
import test.projects.ennov.domain.enums.TicketStatus;
import test.projects.ennov.dto.TicketDto;
import test.projects.ennov.dto.mapper.TicketMapper;
import test.projects.ennov.repository.TicketRepository;
import test.projects.ennov.repository.UserRepository;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class TicketServiceTests {
    @InjectMocks
    TicketService ticketService;
    @Mock
    private TicketMapper mapper;
    @Mock
    private TicketRepository ticketRepository;
    @Mock
    private UserRepository userRepository;

    @Mock
    private TokenProvider tokenProvider;

    @BeforeAll
    public static void init() {
        MockitoAnnotations.openMocks(TicketServiceTests.class);
    }

    @Test
    public void testCreateTicket_thenOK() {
        Ticket ticket = Ticket.builder().title("Test ticket").description("desc").status(TicketStatus.READY).build();
        TicketDto ticketDto = TicketDto.builder().ticketTitle("Test ticket").ticketDesc("desc").ticketStatus(TicketStatus.READY).build();

        when(this.mapper.toEntity(ticketDto)).thenReturn(ticket);
        when(this.ticketRepository.existsByDeletedFalseAndTitle(anyString())).thenReturn(false);

        this.ticketService.createTicket(ticketDto);

        verify(this.ticketRepository, times(1)).save(ticket);
    }

    @Test
    public void testCreateTicket_thenKO() {
        Ticket ticket = Ticket.builder().title("Test ticket").description("desc").status(TicketStatus.READY).build();
        TicketDto ticketDto = TicketDto.builder().ticketTitle("Test ticket").ticketDesc("desc").ticketStatus(TicketStatus.READY).build();
        when(this.mapper.toEntity(ticketDto)).thenReturn(ticket);
        when(this.ticketRepository.existsByDeletedFalseAndTitle(anyString())).thenReturn(true);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> this.ticketService.createTicket(ticketDto));
        verify(this.ticketRepository, times(0)).save(any(Ticket.class));

        Assertions.assertInstanceOf(ResponseStatusException.class, exception);
        Assertions.assertEquals(400, ((ResponseStatusException) exception).getBody().getStatus());
    }

    @Test
    public void testUpdateTicket_thenOK() {
        long id = 1001L;

        Ticket ticket = Ticket.builder().id(id).title("Test ticket").description("desc").status(TicketStatus.READY).build();
        TicketDto ticketDto = TicketDto.builder().id(id).ticketTitle("Test ticket").ticketDesc("desc").ticketStatus(TicketStatus.READY).build();

        when(this.mapper.toEntity(ticketDto)).thenReturn(ticket);
        when(this.ticketRepository.existsByDeletedFalseAndId(id)).thenReturn(true);

        this.ticketService.updateTicket(ticketDto, id);
        verify(this.ticketRepository, times(1)).save(ticket);
    }

    @Test
    public void testUpdateTicket_thenKO_404() {
        long id = 1001L;

        Ticket ticket = Ticket.builder().id(id).title("Test ticket").description("desc").status(TicketStatus.READY).build();
        TicketDto ticketDto = TicketDto.builder().id(id).ticketTitle("Test ticket").ticketDesc("desc").ticketStatus(TicketStatus.READY).build();

        when(this.mapper.toEntity(ticketDto)).thenReturn(ticket);
        when(this.ticketRepository.existsByDeletedFalseAndId(id)).thenReturn(false);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> this.ticketService.updateTicket(ticketDto, id));
        verify(this.ticketRepository, times(0)).save(any(Ticket.class));

        Assertions.assertEquals(404, ((ResponseStatusException) exception).getBody().getStatus());
        Assertions.assertEquals("\"No ticket exists with this ID : 1001\"", exception.getMessage()
                .substring("404 NOT_FOUND ".length()));
    }

    @Test
    public void testUpdateTicket_thenKO_400() {

        Ticket ticket = Ticket.builder().title("Test ticket").description("desc").status(TicketStatus.READY).build();
        TicketDto ticketDto = TicketDto.builder().ticketTitle("Test ticket").ticketDesc("desc").ticketStatus(TicketStatus.READY).build();

        when(this.mapper.toEntity(ticketDto)).thenReturn(ticket);
        when(this.ticketRepository.existsByDeletedFalseAndId(anyLong())).thenReturn(true);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> this.ticketService.updateTicket(ticketDto, anyLong()));
        verify(this.ticketRepository, times(0)).save(any(Ticket.class));

        Assertions.assertEquals(400, ((ResponseStatusException) exception).getBody().getStatus());
        Assertions.assertEquals("\"Already created ticket cannot have null ID.\"", exception.getMessage()
                .substring("400 BAD_REQUEST ".length()));
    }

    @Test
    public void testUpdateTicket_thenKO_409() {

        Ticket ticket = Ticket.builder().title("Test ticket").description("desc").status(TicketStatus.READY).build();
        TicketDto ticketDto = TicketDto.builder().id(1001L).ticketTitle("Test ticket").ticketDesc("desc").ticketStatus(TicketStatus.READY).build();

        when(this.mapper.toEntity(ticketDto)).thenReturn(ticket);

        when(this.ticketRepository.existsByDeletedFalseAndId(anyLong())).thenReturn(true);
        when(this.ticketRepository.existsByDeletedFalseAndTitleAndIdNot(anyString(), anyLong())).thenReturn(true);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> this.ticketService.updateTicket(ticketDto, anyLong()));
        verify(this.ticketRepository, times(0)).save(any(Ticket.class));

        Assertions.assertEquals(409, ((ResponseStatusException) exception).getBody().getStatus());
        Assertions.assertEquals("\"A Ticket with the same title already exists.\"", exception.getMessage()
                .substring("409 CONFLICT ".length()));
    }

    @Test
    public void testDeleteTicket_thenOK() {
        Ticket ticket = Ticket.builder()
                .id(1001L).title("Test ticket").deleted(Boolean.FALSE)
                .description("desc").status(TicketStatus.READY).build();

        when(this.ticketRepository.findTop1ByDeletedFalseAndId(anyLong())).thenReturn(Optional.of(ticket));

        this.ticketService.deleteTicket(1001L);

        verify(this.ticketRepository, times(1)).save(ticket);
        Assertions.assertTrue(ticket.getDeleted());
    }


    @Test
    public void testDeleteTicket_thenKO() {
        when(this.ticketRepository.findTop1ByDeletedFalseAndId(anyLong())).thenReturn(Optional.empty());

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> this.ticketService.deleteTicket(1000L));
        verify(this.ticketRepository, times(0)).save(any(Ticket.class));

        Assertions.assertEquals(400, ((ResponseStatusException) exception).getBody().getStatus());
        Assertions.assertEquals("\"Cannot remove ticket with ID : 1000\"", exception.getMessage()
                .substring("400 BAD_REQUEST ".length()));
    }

    @Test
    public void testAssignTicket_thenOK() {
        Ticket ticket = Ticket.builder()
                .id(1001L).title("Test ticket").deleted(Boolean.FALSE)
                .description("desc").status(TicketStatus.READY).build();

        when(this.userRepository.existsByDeletedFalseAndId(anyLong())).thenReturn(true);
        when(this.ticketRepository.existsByDeletedFalseAndStatusAndId(any(TicketStatus.class), anyLong())).thenReturn(false);
        when(this.ticketRepository.getReferenceById(anyLong())).thenReturn(ticket);

        this.ticketService.assignTicket(1001L, 1002L);

        verify(this.ticketRepository, times(1)).save(ticket);
        Assertions.assertEquals(1002, ticket.getUser().getId());
    }

    @Test
    public void testAssignTicket_thenKO_400() {
        when(this.userRepository.existsByDeletedFalseAndId(anyLong())).thenReturn(false);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> this.ticketService.assignTicket(1001L, 1001L));
        verify(this.ticketRepository, times(0)).save(any(Ticket.class));
        verify(this.userRepository, times(0)).getReferenceById(anyLong());

        Assertions.assertEquals(400, ((ResponseStatusException) exception).getBody().getStatus());
        Assertions.assertTrue(exception.getMessage().contains("No user exists with this ID"));
    }

    @Test
    public void testAssignTicket_thenKO_403() {
        when(this.userRepository.existsByDeletedFalseAndId(anyLong())).thenReturn(true);
        when(this.ticketRepository.existsByDeletedFalseAndStatusAndId(any(TicketStatus.class), anyLong())).thenReturn(true);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> this.ticketService.assignTicket(1001L, 1001L));
        verify(this.ticketRepository, times(0)).save(any(Ticket.class));
        verify(this.userRepository, times(0)).getReferenceById(anyLong());

        Assertions.assertEquals(403, ((ResponseStatusException) exception).getBody().getStatus());
        Assertions.assertTrue(exception.getMessage().contains("Cannot assign again a closed ticket."));
    }

    @Test
    public void TestFetchByCurrentUser_thenOK() {
        TestUtils.login();
        User user = User.builder().id(1001L).username("dbadmin").password("admin").build();

        List<Ticket> tickets = List.of(
                Ticket.builder().id(1000L).title("Test Ticket 1").description("desc 1").deleted(Boolean.FALSE).user(User.builder().id(1001L).build()).build(),
                Ticket.builder().id(1001L).title("Test Ticket 2").description("desc 2").deleted(Boolean.FALSE).user(User.builder().id(1001L).build()).build()
        );
        when(ticketRepository.findAllByDeletedFalseAndUserId(user.getId())).thenReturn(tickets);

        when(userRepository.findTop1ByDeletedFalseAndUsername("dbadmin")).thenReturn(user);
        List<TicketDto> result = ticketService.fetchTicketByCurrentUser();
        Assertions.assertNotNull(result);
        Assertions.assertNotEquals(2, result.size());
    }

    @Test
    public void TestFetchByCurrentUser_thenKO() {
        SecurityContextHolder.getContext().setAuthentication(null);
        ResponseStatusException exception = Assertions.assertThrows(ResponseStatusException.class, () -> ticketService.fetchTicketByCurrentUser());

        Assertions.assertEquals(403,  exception.getBody().getStatus());
        Assertions.assertTrue(exception.getMessage().contains("You are not logged in."));
    }

}
