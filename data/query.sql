INSERT INTO ENNOV_AUTHORITY
VALUES ('ADMIN'),
       ('DEV'),
       ('PO');

INSERT INTO ENNOV_USERS (id, username, passwd, created_by, created_date, deleted, email)
VALUES (1001, 'dbuser', '$2y$10$.qkbukzzX21D.bqbI.B2R.tvWP90o/Y16QRWVLodw51BHft7ZWbc.', 'system', now(), false,
        'dieudo.gov.bf'),
       (1002, 'dbadmin', '$2y$10$kp1V7UYDEWn17WSK16UcmOnFd1mPFVF6UkLrOOCGtf24HOYt8p1iC', 'system', now(), false,
        'roxana.gov.bf');


INSERT INTO ENNOV_USER_AUTHORITY
VALUES (1001, 'ADMIN'),
       (1001, 'DEV'),
       (1001, 'PO'),
       (1002, 'DEV'),
       (1002, 'PO')